# Contribution / Bijdragen.
Omdat dit een open-source project is, vindt ik het geweldig als andere hun bijdrage insturen.  
Bijdragen leveren kan op verschillende manier.

- Bugs en fouten raporteren.
- Discussies over de huidige source-code.
- Pull Requests met oplossingen voor problemen.
- Insturen van ideeën en aanpassingen.
- Onderhouder worden (Maintainer).

## GitLab is waar het gebeurd.
Ik gebruik GitLab om de source-code te hosten, issues en ideeën te ontvangen en de Pull Requests.

## Raporteren.
Bugs en fouten kunnen geraptoreerd worden via GitLab Issues.  
Bug insturen? [Klik dan hier](/-/issues).

**De betere bug reports voldoen aan..**
- Een kleine beschrijving met de achtergrond van het probleem.
- Uitleg hoe het probleem te reproduceren.
- Uitleg over wat je verwacht dat zou moeten gebeuren.
- Uitleg over wat er precies gebeurde.
- Evenueel een schermafbeelding.

## Code stijl
Dit project, net als vele andere projecten die ik onderhoud
gebruiken **2 spaces** als indent.  
Wanneer je een wijziging wil aanbieden, weet dat zeker dat jou code diezelfde indent stijl gebruikt.

**Meer informatie volgt later**