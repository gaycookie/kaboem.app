import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '/list',
    name: 'List',
    component: () => import('../views/List')
  }, {
    path: '/logout',
    name: 'Logout',
    component: () => import('../views/auth/Logout')
  }, {
    path: '/auth/discord',
    name: 'Discord_Auth',
    component: () => import('../views/auth/Discord')
  }, {
    path: '/auth/discord/callback', 
    name: 'Discord_Callback',
    props: route => ({ access_token: route.query.access_token }),
    component: () => import('../views/auth/callbacks/Discord')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
