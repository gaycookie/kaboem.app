import Vue from 'vue'
import VueCookie from 'vue-cookie'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.use(VueCookie)

import '@materializecss/materialize/sass/materialize.scss'
import './assets/styles/main.sass'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
