[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/kawaaii/kaboem.app/master?style=for-the-badge)](https://gitlab.com/kawaaii/kaboem.app/-/jobs)‎‎‎‎‎‏‏‎‏‏‎‏‏‎ ‎
[![Netlify](https://img.shields.io/netlify/318b2b83-065c-4ff4-b64c-f79576a3420d?style=for-the-badge)](https://kaboem.kawaaii.moe)
‎‎‎‎
# Kaboem.app
Meer informatie over deze repository volgt later!

# Contributie
Voel je je geroepen om te helpen met het verbeteren van **Kaboem.app**?  
Dan ben ik daar heel erb blij mee gezien ik alles zelfstandig doe, is alle hulp welkom!  
Waar met name hulp is gezocht, is met spelfouten/gramatica.

Meer informatie over contributie komt later!